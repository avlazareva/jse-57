package ru.t1.lazareva.tm.exception.user;

public class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

}

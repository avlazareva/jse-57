package ru.t1.lazareva.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.exception.AbstractException;

public abstract class AbstractEntityException extends AbstractException {

    @SuppressWarnings("unused")
    public AbstractEntityException() {
    }

    public AbstractEntityException(@NotNull final String message) {
        super(message);
    }

    @SuppressWarnings("unused")
    public AbstractEntityException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    @SuppressWarnings("unused")
    public AbstractEntityException(@NotNull final Throwable cause) {
        super(cause);
    }

    @SuppressWarnings("unused")
    public AbstractEntityException(@NotNull final String message, @NotNull final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
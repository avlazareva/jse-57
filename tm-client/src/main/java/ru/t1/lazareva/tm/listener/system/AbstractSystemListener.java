package ru.t1.lazareva.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.lazareva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.lazareva.tm.api.service.IListenerService;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.listener.AbstractListener;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IListenerService commandService;

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

}
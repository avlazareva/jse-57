package ru.t1.lazareva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.lazareva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.lazareva.tm.dto.model.TaskDto;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.listener.AbstractListener;

import java.util.List;

@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showTask(@Nullable final TaskDto task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    protected void renderTasks(@NotNull final List<TaskDto> tasks) {
        int index = 1;
        for (@Nullable TaskDto task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task.getName());
            index++;
        }
    }

}
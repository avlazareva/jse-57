package ru.t1.lazareva.tm.service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.repository.IListenerRepository;
import ru.t1.lazareva.tm.api.service.IListenerService;
import ru.t1.lazareva.tm.listener.AbstractListener;

import java.util.Collection;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class CommandService implements IListenerService {

    @NotNull
    @Autowired
    private IListenerRepository commandRepository;

    @Override
    public void add(@Nullable final AbstractListener command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Nullable
    @Override
    public AbstractListener getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @Nullable
    @Override
    public AbstractListener getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) return null;
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @NotNull
    @Override
    public Iterable<AbstractListener> getCommandsWithArgument() {
        return commandRepository.getCommandsWithArgument();
    }

}
package ru.t1.lazareva.tm.api.client;

import org.jetbrains.annotations.NotNull;
import ru.t1.lazareva.tm.dto.request.*;
import ru.t1.lazareva.tm.dto.response.*;

public interface IDomainEndpointClient extends IEndpointClient {

    @NotNull
    DataBackupLoadResponse loadBackup(@NotNull DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse saveBackup(@NotNull DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse loadBase64(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse saveBase64(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse loadBinary(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse saveBinary(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataJsonLoadFasterXmlResponse loadJsonFasterXml(@NotNull DataJsonLoadFasterXmlRequest request);

    @NotNull
    DataJsonSaveFasterXmlResponse saveJsonFasterXml(@NotNull DataJsonSaveFasterXmlRequest request);

    @NotNull
    DataJsonLoadJaxBResponse loadJsonJaxB(@NotNull DataJsonLoadJaxBRequest request);

    @NotNull
    DataJsonSaveJaxBResponse saveJsonJaxB(@NotNull DataJsonSaveJaxBRequest request);

    @NotNull
    DataXmlLoadFasterXmlResponse loadXmlFasterXml(@NotNull DataXmlLoadFasterXmlRequest request);

    @NotNull
    DataXmlSaveFasterXmlResponse saveXmlFasterXml(@NotNull DataXmlSaveFasterXmlRequest request);

    @NotNull
    DataXmlLoadJaxBResponse loadXmlJaxB(@NotNull DataXmlLoadJaxBRequest request);

    @NotNull
    DataXmlSaveJaxBResponse saveXmlJaxB(@NotNull DataXmlSaveJaxBRequest request);

    @NotNull
    DataYamlLoadFasterXmlResponse loadYamlFasterXml(@NotNull DataYamlLoadFasterXmlRequest request);

    @NotNull
    DataYamlSaveFasterXmlResponse saveYamlFasterXml(@NotNull DataYamlSaveFasterXmlRequest request);

}


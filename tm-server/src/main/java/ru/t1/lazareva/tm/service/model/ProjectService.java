package ru.t1.lazareva.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.lazareva.tm.api.repository.model.IProjectRepository;
import ru.t1.lazareva.tm.api.service.model.IProjectService;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.lazareva.tm.exception.field.*;
import ru.t1.lazareva.tm.model.Project;
import ru.t1.lazareva.tm.model.User;

import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    protected IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @Override
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        if (status != null)
            project.setStatus(status);
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(userId, project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        if (status != null)
            project.setStatus(status);
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(userId, project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        project.setUser(entityManager.find(User.class, userId));
        try {
            entityManager.getTransaction().begin();
            repository.add(userId, project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        project.setUser(entityManager.find(User.class, userId));
        try {
            entityManager.getTransaction().begin();
            repository.add(userId, project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.update(userId, project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}

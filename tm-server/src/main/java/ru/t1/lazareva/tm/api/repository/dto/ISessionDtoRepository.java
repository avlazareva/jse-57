package ru.t1.lazareva.tm.api.repository.dto;

import ru.t1.lazareva.tm.dto.model.SessionDto;

public interface ISessionDtoRepository extends IUserOwnedDtoRepository<SessionDto> {
}

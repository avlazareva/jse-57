package ru.t1.lazareva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Task create(@NotNull String userId, @NotNull String name, @NotNull String description) throws Exception;

    @NotNull
    Task create(@NotNull String userId, @NotNull String name) throws Exception;

    @Nullable
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

    void removeAllByProjectId(@NotNull String userId, @NotNull String projectId);

}


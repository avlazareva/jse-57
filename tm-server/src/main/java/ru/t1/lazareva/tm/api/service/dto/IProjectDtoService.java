package ru.t1.lazareva.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.enumerated.Status;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDto> {

    ProjectDto changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

    ProjectDto changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws Exception;

    @NotNull
    ProjectDto create(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    ProjectDto create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    ProjectDto updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    ProjectDto updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;

}